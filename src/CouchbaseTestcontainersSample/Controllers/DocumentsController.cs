using System;
using Microsoft.AspNetCore.Mvc;

namespace CouchbaseTestcontainersSample.Controllers
{
    [ApiController]
    [Route("/document")]
    public class DocumentsController
    {
        [HttpGet]
        public Health Check()
        {
            return new Health
            {
                Date = DateTime.Now,
                Status = "UP"
            };
        }
    }
}