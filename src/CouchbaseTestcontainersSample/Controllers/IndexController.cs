using System;
using Microsoft.AspNetCore.Mvc;

namespace CouchbaseTestcontainersSample.Controllers
{
    [ApiController]
    [Route("/")]
    public class IndexController : ControllerBase
    {
        [HttpGet]
        public Health Check()
        {
            return new Health
            {
                Date = DateTime.Now,
                Status = "UP"
            };
        }
    }
}