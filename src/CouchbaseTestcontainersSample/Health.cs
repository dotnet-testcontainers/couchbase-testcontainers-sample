using System;

namespace CouchbaseTestcontainersSample
{
    public class Health
    {
        public DateTime Date { get; set; }

        public string Status { get; set; }
    }
}
