using System.Threading.Tasks;
using CouchbaseTestcontainersSample.Domain;

namespace CouchbaseTestcontainersSample.Interfaces
{
    public interface IDocumentsRepository
    {
        Task<Document> FindById(string id);
        Task Save(Document document);
    }
}