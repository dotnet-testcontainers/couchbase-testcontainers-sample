using System;
using System.Threading.Tasks;
using Couchbase;
using Microsoft.Extensions.Configuration;

namespace CouchbaseTestcontainersSample.Providers
{
    public class CouchbaseProvider : ICouchbaseProvider
    {

        private ICluster _cluster;
        private IBucket _bucket;
        private readonly IConfiguration _configuration;
        

        public CouchbaseProvider(IConfiguration configuration)
        {
            _configuration = configuration;

        }

        public async Task<ICluster> GetCluster()
        {
            if (_cluster != null) return _cluster;
            
            var hosts = _configuration.GetValue<string>("CouchbaseHosts");
            var username = _configuration.GetValue<string>("CouchbaseUsername");
            var password = _configuration.GetValue<string>("CouchbasePassword");
            var uiPort = _configuration.GetValue<int>("CouchbaseUIPort");

            _cluster = await Cluster.ConnectAsync($"{hosts}", new ClusterOptions
            {

                BootstrapHttpPort = uiPort,
                UserName = username,
                Password = password
            });

            return _cluster;
        }

        public async Task<IBucket> GetBucket()
        {
            if (_bucket == null)
            {
                var bucketName = _configuration.GetValue<string>("CouchbaseBucketName");
                var cluster = await GetCluster();
                _bucket = await cluster.BucketAsync(bucketName);
            }

            return _bucket;
        }
    }

}