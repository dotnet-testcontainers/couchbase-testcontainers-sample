using System.Threading.Tasks;
using Couchbase;

namespace CouchbaseTestcontainersSample.Providers
{
    public interface ICouchbaseProvider
    {
        Task<ICluster> GetCluster();
        Task<IBucket> GetBucket();
    }
}