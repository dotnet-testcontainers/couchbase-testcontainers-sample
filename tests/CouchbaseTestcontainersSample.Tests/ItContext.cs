using System;
using DotNet.Testcontainers.Containers.Builders;
using DotNet.Testcontainers.Containers.Configurations.Databases;
using DotNet.Testcontainers.Containers.Modules.Databases;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

[assembly: CollectionBehavior(DisableTestParallelization = true)]
namespace CouchbaseTestcontainersSample.Tests
{
    public class ItContext : IDisposable
    {
        public CouchbaseTestcontainer Container { get; set; }

        private IServiceCollection _serviceCollection;
        private IConfiguration _configuration;
        private IServiceScope _serviceScope;
        
        public ItContext()
        {
            _serviceCollection = new ServiceCollection();
            CreateCouchbaseContainer();
            Configure();
            CreateStartup();
            CreateScope();
        }

        public T GetRequiredService<T>()
        {
            return (T)_serviceScope.ServiceProvider.GetRequiredService(typeof(T));
        }

        private void CreateCouchbaseContainer()
        {
            Container = new TestcontainersBuilder<CouchbaseTestcontainer>()
                            .WithDatabase(new CouchbaseTestcontainerConfiguration
                            {
                                Username = "Administrator",
                                Password = "password",
                                BucketName = "Documents"
                            })
                            .WithExposedPort(8091)
                            .WithExposedPort(8093)
                            .WithExposedPort(11210)
                            .WithPortBinding(8091, 8091)
                            .WithPortBinding(8093, 8093)
                            .WithPortBinding(11210, 11210)
                            .Build();
            
            Container.StartAsync().ConfigureAwait(false).GetAwaiter().GetResult();
            
        }

        private void Configure()
        {
            _configuration = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings-test.json").Build();
            _configuration["CouchbaseHosts"] = Container.ConnectionString;
            _configuration["CouchbaseUsername"] = Container.Username;
            _configuration["CouchbasePassword"] = Container.Password;
            _configuration["CouchbaseUIPort"] = Container.Port.ToString();
            _serviceCollection.AddSingleton(_configuration);
        }
        
                
        private void CreateStartup()
        {
            new Startup(_configuration).ConfigureServices(_serviceCollection);
        }
        
        private void CreateScope()
        {
            _serviceScope = _serviceCollection.BuildServiceProvider().CreateScope();
        }

        public void Dispose()
        {
            Container.DisposeAsync();
        }
    }
}



