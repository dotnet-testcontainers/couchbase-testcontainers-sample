using CouchbaseTestcontainersSample.Domain;
using CouchbaseTestcontainersSample.Interfaces;
using CouchbaseTestcontainersSample.Providers;
using Xunit;
using FluentAssertions;

namespace CouchbaseTestcontainersSample.Tests.Repositories
{
    [Trait("Type", "Integration")]
    [Collection("ItContext")]
    public class DocumentsRepositoryIt
    {
        private ItContext _context;

        public DocumentsRepositoryIt(ItContext context)
        {
            _context = context;
        }

        [Fact]
        public async void Save_ItShouldSaveDocument()
        {
            //given
            Document sampleDocument = new Document
            {
                Id = "Document001",
                Data = "Sample document for Save_ItShouldSaveDocument test."
            };

            //when
            await _context.GetRequiredService<IDocumentsRepository>().Save(sampleDocument);

            //then
            var couchbaseProvider = _context.GetRequiredService<ICouchbaseProvider>();
            var collection = (await couchbaseProvider.GetBucket()).DefaultCollection();
            Document actualSampleDocument = (await collection.GetAsync("Document001")).ContentAs<Document>();

            actualSampleDocument.Id.Should().Be("Document001");
            actualSampleDocument.Data.Should().Be("Sample document for Save_ItShouldSaveDocument test.");
        }
        
        [Fact]
        public async void FindById_ItShouldFindDocumentWithId()
        {
            //Given
            var documentsRepository = _context.GetRequiredService<IDocumentsRepository>();
            Document sampleDocument = new Document
            {
                Id = "Document002",
                Data = "Sample document for FindById_ItShouldFindDocumentWithId test."
            };
            await documentsRepository.Save(sampleDocument);
            
            //When
            var document = await documentsRepository.FindById("Document002");
            
            //Then
            document.Id.Should().Be("Document002");
            document.Data.Should().Be("Sample document for FindById_ItShouldFindDocumentWithId test.");
        }
    }
    
}